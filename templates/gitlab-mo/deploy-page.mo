<!DOCTYPE html>
<html>
<head>
	<title>Warlpiri mini dictionary deploy page</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="container">

	<h1>Deployment information</h1>
	Produced on commit {{CI_COMMIT_SHA}} on {{DATE}} by pipeline <a href="https://gitlab.com/coedl/mini-wlp/pipelines/{{CI_PIPELINE_ID}}">{{CI_PIPELINE_ID}}</a>.

	<h1>Downloads</h1>

	<ul>
		<li>PDF dictionary: <a href="wlp-lexicon.pdf">wlp-lexicon.pdf</a></li>
		<li>Web app: <a href="wlp-lexicon.html">wlp-lexicon.html</a></li>
		<li>InDesign XML: <a href="wlp-xml.zip">wlp-xml.zip</a></li>
	</ul>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
