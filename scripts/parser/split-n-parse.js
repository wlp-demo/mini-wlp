const fs           = require('fs')
const path         = require('path')
const nearley      = require('nearley')

// Set up options for command-line use
var   opts         = require("nomnom")

	.option('grammar', {
		abbr: 'g',
		required: true,
		default: 'grammar.js',
		help: 'A compiled Nearley parser grammar'
	  })

	.option('data', {
		abbr: 'd',
		required: true,
		default: 'data.txt',
		help: 'Data to be parsed by the grammar'
	})

	.option('split', {
		abbr: 's',
		help: 'Regular expression by which to split the data (optional)'
	})

	.option('name', {
		abbr: 'n',
		required: true,
		help: 'Top-level name for results object'
	})

	.parse()

// Read in grammar, and set up parser function from the grammar
const grammar      = require(path.resolve(opts.grammar))

const parse_step   = (parse_chunk) => {
	
	var parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar))

	try {
		parser.feed(parse_chunk)
	} catch(parse_error) {
		const error_string = parse_error.stack.split(/\n/).slice(0, 6).join("\n");

		console.error("\n File: " + opts.data + "\n\n" + parse_chunk.split(/\n/)[0] + "\n\n" + error_string)

		return { "error_entry": parse_chunk.split(/\n/)[0], "error" : error_string }
	}

	if (parser.results.length == 0) {
		console.error("\n File: " + opts.data + "\n\n" + parse_chunk.split(/\n/)[0] + "\n\n" + "Incomplete parse. Expecting more at end of input.")

		return { "error_entry": parse_chunk.split(/\n/)[0], "error": "Incomplete parse. Expecting more at end of input." }
	}

	return parser.results	
}

// Read in data
const data         = fs.readFileSync(path.resolve(opts.data)).toString()

// If there is a split set, then split and parse each chunk
// If not passs through entire chunk to a single instance of the parser
var split_re       = new RegExp(opts.split)

let return_object        = {}

return_object[opts.name] = !opts.split ? parse_step(data) : data.split(split_re).map(parse_step)

// if("error" in return_object[opts.name]) { fs.appendFileSync(path.resolve(opts.data), "\n\n" + return_object[opts.name].error); }

process.stdout.write(JSON.stringify(return_object, null, 2))
